import requests
from bs4 import BeautifulSoup
import json
import csv
import datetime

URL_AZURE = 'https://azure.microsoft.com/en-us/pricing/details/storage/data-lake/'
URL_AMAZON = 'https://a0.p.awsstatic.com/pricing/1.0/s3/index.json'
URL_GOOGLE = 'https://cloud.google.com/storage/pricing'

CSV_FILE_PATH = '../data/cloud-storage-historical-prices.csv'

def scrape_azure_data():

    ###########################################################################
    ############################### MS Azure Scraping #########################
    ###########################################################################

    # The table on the Azure website is inside a table. We need to get the data 
    # table tag (td). Inside the data table tag (td) there is a span with a 
    # attribute called data-amount. This attribute has all the prices available
    # A js code reads the data-amount and change the text of the span based 
    # on some variables

    response = requests.get(URL_AZURE)
    soup = BeautifulSoup(response.content, "html.parser")

    tables = soup.find_all('table') # find all tables
    price_table = tables[0] # get the target table

    ### Getting the headers
    headers = price_table.findChildren(['th']) # find headers in the table
    type_of_service_azure = headers[1].string # get the target header

    ### Getting the table data
    rows = price_table.findChildren(['tr']) # find all rows in the table
    row = rows[7] # getting the target row
    cells = row.findChildren('td') # getting the all of data table in the row
    cell = cells[1] # getting the target cell
    span = cell.findChildren('span') # data is in an attribute of span tag
    data = span[0].attrs['data-amount'] #getting the attribute (json structure)

    json_data = json.loads(data)
    service_value_azure = json_data['regional']['us-central']

    service_value_azure = "{:.3f}".format(float(service_value_azure))

    #print(f"Type of service Azure: {type_of_service_azure}")
    #print(f"Price of service Azure: {service_value_azure}")

    return service_value_azure

def scrape_amazon_data():

    ###########################################################################
    ############################ Amazon S3 Scraping ###########################
    ###########################################################################

    # Amazon uses an API service the return all the prices used in the website.

    response = requests.get(URL_AMAZON)
    json_data = json.loads(response.content)

    ### offer id that we need to find
    ### id founded in the html table
    offer_id = "YPGKVRB2EKTVDJDT.JRTCKXETXF.PGHJ3S3EYE"

    type_of_service_amazon = ""
    service_value_amazon = 0

    prices = json_data['prices']
    for price in prices:

        if price['id'] == offer_id:
            service_value_amazon = price['price']['USD']
            type_of_service_amazon = price['attributes']['aws:service']

    service_value_amazon = "{:.3f}".format(float(service_value_amazon))

    #print(f"Type of service Amazon: {type_of_service_amazon}")
    #print(f"Price of service Amazon: {service_value_amazon}")

    return service_value_amazon

def scrape_google_data():

    ################################################################################################
    ################################### Google Cloud Scraping ##########################################
    ################################################################################################

    response = requests.get(URL_GOOGLE)
    soup = BeautifulSoup(response.content, "html.parser")

    tables = soup.find_all("table")
    target_table = tables[6]

    table_data = target_table.findChildren("td")
    table_data_target = table_data[1]

    lst_str = table_data_target.text.split(" ")

    type_of_service_google = lst_str[2]
    service_value_google = lst_str[5]
    service_value_google = service_value_google.replace('$','')
    service_value_google = "{:.3f}".format(float(service_value_google))

    #print(f"Type of service Amazon: {type_of_service_google}")
    #print(f"Price of service Amazon: {service_value_google}")

    return service_value_google

def retrieve():
    #Get the data from the url and cache it.

    service_value_azure = scrape_azure_data()
    service_value_amazon = scrape_amazon_data()
    service_value_google = scrape_google_data()

    data = {"azure_data" : service_value_azure,
            "amazon_data" : service_value_amazon,
            "google_data": service_value_google
    }

    return data

def extract_and_transform(data):
    # load the csv, standardize ... tidy ...
    
    service_value_azure = data["azure_data"]
    service_value_amazon = data["amazon_data"]
    service_value_google = data["google_data"]

    now = datetime.datetime.now()
    current_year = now.year

    writer_rows = []
    with open(CSV_FILE_PATH, newline='') as csvfile:
        reader = csv.DictReader(csvfile)

        headers = reader.fieldnames
        writer_rows.append({'Year' : headers[0], 
                            'Amazon' : headers[1],
                            'Azure' : headers[2],
                            'Google' : headers[3]
                            })

        years = []
        for row in reader:

            years.append(int(row['Year']))

            dictRow = {}
            if int(row['Year']) == current_year: 
                #update current year values
                dictRow['Year'] = current_year
                dictRow['Amazon'] = service_value_amazon
                dictRow['Azure'] = service_value_azure
                dictRow['Google'] = service_value_google
            else:
                #keep values
                dictRow['Year'] = row['Year']
                dictRow['Amazon'] = row['Amazon']
                dictRow['Azure'] = row['Azure']
                dictRow['Google'] = row['Google']
        
            writer_rows.append(dictRow)

        if current_year not in years:
            # insert a new year that is not in the list
            dictRow = {}
            dictRow['Year'] = current_year
            dictRow['Amazon'] = service_value_amazon
            dictRow['Azure'] = service_value_azure
            dictRow['Google'] = service_value_google
            writer_rows.append(dictRow)

    return writer_rows

def load(writer_rows):
    # save data frames in csv files
    
    with open(CSV_FILE_PATH, 'w', newline='') as f:
        writer = csv.writer(f)
        for row in writer_rows:
            writer.writerow(row.values())

def run():
    data = retrieve()
    writer_rows = extract_and_transform(data)
    load(writer_rows)

if __name__ == '__main__':
    run()



