# Annual Time Series of Cloud Storage Pricing

The pricing of different cloud storage services in an annual time series from 2010 to 2019.
Three storage services are compared: Amazon S3, MS Azure and Google Cloud.

## Data

Data comes from the providers' websites, from past snapshots of the providers' websites (using [WebArchive]), and other analyses found by research on the internet. The data were collected by the authors and combined in a dataset.

The time series is annual and storage prices are in USD($) per GigaByte.

Source: [Amazon], [Azure], [Google], and [Storj].

## Preparation

The data is ready to use. No preparation is needed.

### Updating

There is a script inside the script directory to update the data. The script creates an updated file in the data package directory. The script updates the data of the current year in the CSV file. If the current year is not in the CSV file, then the script creates a new line to insert current prices. The required Python packages to run the script are listed in the file requirements.txt and can be installed by pip. 

```
pip install -r requirements.txt
```

The script can be running using python.

```
python process.py
```

## License

This Data Package is made available under the Public Domain Dedication and License v1.0 whose full text can be found at: [PPDL]

[PPDL]: http://www.opendatacommons.org/licenses/pddl/1.0/
[Amazon]: https://aws.amazon.com/s3/pricing/
[Azure]: https://azure.microsoft.com/en-us/pricing/details/storage/data-lake/
[Google]: https://cloud.google.com/storage/pricing
[Storj]: https://storj.io/blog/2018/11/the-high-price-of-traditional-cloud-storage/
[WebArchive]: http://web.archive.org/
